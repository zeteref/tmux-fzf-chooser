#!/bin/bash

self="$(readlink -f "$0")"
preview_script="$(dirname $(readlink -f "$0"))/scripts/preview"
SEPARATOR=$'\u250A'

SWITCH_POPUP_MARK=/tmp/.fzf.chooser.tab.mark
ACTIVE_CHAR=$'\u25cf'
INACTIVE_CHAR=$'\u21b3'

export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS --layout reverse"

declare -A sessions
CURRENT_SESSION_F=/tmp/.fzf-chooser.tmux.curr_session
PREVIOUS_SESSION_F=/tmp/.fzf-chooser.tmux.prev_session
touch $CURRENT_SESSION_F $PREVIOUS_SESSION_F

CURRENT_SESSION=$(<$CURRENT_SESSION_F)
PREVIOUS_SESSION=$(<$PREVIOUS_SESSION_F)

function u:read-json-obj-to-assoc-arr() {
    declare -A myarray
    while IFS="=" read -r key value
    do
        myarray[$key]="$value"
    done < <(jq -r 'to_entries|map("(.key)=(.value)")|.[]' file)
}

function u:list-sessions() {
    local sess; readarray -t sess <<< "$(tmux list-sessions -F '#S' | sort )"
    local sorted=()

    if [[ "$PREVIOUS_SESSION" != "" ]]; then
        sorted+=( "$PREVIOUS_SESSION" )
    fi

    for i in "${sess[@]}"; do
        if [[ "$i" != "$PREVIOUS_SESSION" ]]; then
            sorted+=( "$i" )
        fi
    done

    printf "%s\n" "${sorted[@]}"
}

function u:switch-client() {
    if [[ "$CURRENT_SESSION" == "$1" ]]; then
        return
    fi
    printf '%s' "$CURRENT_SESSION" > "$PREVIOUS_SESSION_F"
    printf '%s' "$1" > "$CURRENT_SESSION_F"
    tmux switch-client -t "$1"
}

get-tmux-option() {
	tmux show-option -gqv "$1"
}

should-switch-popup() {
    test -f "$SWITCH_POPUP_MARK"
    ret=$?
    rm -f "$SWITCH_POPUP_MARK"
    return $ret
}


format-window-line() {
    IFS="$SEPARATOR" read -r id pane_id cmd title pane_path no_panes session_name active_window_index <<< "$1"

    if [[ "$active_window_index" != "$id" ]]; then
        printf " %s %s" "$INACTIVE_CHAR" "$id"
    else
        echo -n " $ACTIVE_CHAR $id"
    fi
    
    if (( no_panes > 1 )); then
        echo -n " [$no_panes]"
    fi

    echo -n " \"$title\""

    if [[ "$pane_path" != "$SESSION_PATH" ]]; then
        echo -n " (${pane_path//${SESSION_PATH}\//./})"
    fi
    echo
}


list-tmux-windows() {
    if [[ "$1" == "--reverse" ]]; then
        filter=tac
        shift
    else
        filter=cat
    fi
    S=$SEPARATOR
    SESSION_NAME="${1}"
    SESSION_PATH="$(tmux display-message -t "$SESSION_NAME" -p '#{session_path}')"
    ACTIVE_WIN="$(tmux display-message -t "$SESSION_NAME" -p '#{active_window_index}')"

    echo "$SESSION_NAME - ${ACTIVE_CHAR}$ACTIVE_WIN ($SESSION_PATH)"  # We use reverse-list layout so this will be on top
    tmux list-windows -t "$SESSION_NAME" -F "#I$S#D$S#W$S#T$S#{pane_current_path}$S#{window_panes}$S#S$S#{active_window_index}" | $filter | while IFS= read -r line
    do
        format-window-line "$line"
    done 
}

select-session() {
    u:list-sessions \
        | fzf \
            --header "Sessions:" \
            --header-first \
            --preview="$self --list-tmux-windows {}" \
            --preview-window right,80% \
            --bind="tab:execute(tmux switch-client -t {1} && touch $SWITCH_POPUP_MARK)+abort" \
    | xargs -r $self --switch-client
}

select-window() {
    SESSION_NAME="$(tmux display-message -p '#S')"
    list-tmux-windows --reverse "$SESSION_NAME" \
        | fzf \
            --header-lines=1 \
            --header-first \
            --no-sort \
            --delimiter=' ' \
            --preview="$self --display-preview-window '$SESSION_NAME:{3}'" \
            --bind="tab:execute(touch $SWITCH_POPUP_MARK)+abort" \
            --tac \
            +m \
            --preview-window bottom,follow \
            --height 100% \
        | grep "$INACTIVE_CHAR" \
        | cut -f3 -d' ' \
    | xargs -r tmux select-window -t
}

bind-key() {
    local key="$(get-tmux-option "$1")"
    if test -n "$key"; then
        tmux bind "$key" "${@:2}" 
    fi
}

display-session-popup() {
    tmux display-popup -w 85% -h 85% -E "$self --select-session"
    should-switch-popup && $self --display-window-popup
    return 0
}

display-window-popup() {
    tmux display-popup -w 85% -h 85% -E "$self --select-window"
    should-switch-popup && $self --display-session-popup
    return 0
}


display-preview-window() {
    session_win_id="$1"
    tmpfiles=()
    trap 'rm -f "${tmpfiles[@]}"' EXIT
    for pane_id in $(tmux list-panes -F '#D' -t "$session_win_id")
    do
        tmpfile="$(mktemp /tmp/fzf-chooser.tmux.XXX)"
        tmux capture-pane -pe -t "$pane_id" | tac | sed '/\S/,$!d' | tac > "$tmpfile"
        tmpfiles+=( "$tmpfile" )
    done

    $preview_script "${tmpfiles[@]}"
}

main() {
    bind-key @fzf-chooser.bind.sessions run-shell "$self --display-session-popup"
    bind-key @fzf-chooser.bind.windows run-shell "$self --display-window-popup"
    bind-key @fzf-chooser.bind.tree choose-tree
}

help() {
    cat <<- EOF
	Usage: $0 [option]

	Options:
	    --display-window-popup 
	    --display-session-popup
	    --select-window
	    --select-session
	    --list-tmux-windows
	EOF
}

case "$1" in
    "")
        main
        ;;
    --display-session-popup)
        display-session-popup
        ;;
    --display-window-popup)
        display-window-popup
        ;;
    --select-session)
        select-session 
        ;;
    --select-window)
        select-window 
        ;;
    --list-tmux-windows)
        list-tmux-windows "$2"
        ;;
    --display-preview-window)
        display-preview-window "${2//$ACTIVE_CHAR}"
        ;;
    --switch-client)
        u:switch-client "${@:2}"
        ;;
    --help)
        help
        ;;
    *)
        echo "Invalid options $*" >&2
        exit 1
    ;;
esac
